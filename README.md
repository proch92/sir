# Sistemi Intelligenti Robotici
Course lab code and course final projects

### Course final projects in Reinforcement Learning
[Markov descrete Q-Learning](project/rl/MDP-QL/readme.md)
[Baselines DQN testing](project/rl/DQN/readme.md)
